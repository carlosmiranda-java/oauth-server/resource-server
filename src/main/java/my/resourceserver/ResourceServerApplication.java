package my.resourceserver;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerApplication {
	
	
	public static void main(String[] args) {
		SpringApplication.run(ResourceServerApplication.class, args);
	}
	

	
	@RequestMapping("/secureCall")
	@PreAuthorize("hasRole('CUSTOM')")
	public String securedCall() {
    	Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		return "user:" + user;
	}
	
	
		
	@RequestMapping(value={"/healthz"})
	public String test() {
    	return "200 ok";
	}
	
		
}

